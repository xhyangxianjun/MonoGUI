
// ChildView.cpp : CChildView 类的实现
//

#include "stdafx.h"
#include "BmpToMatrix.h"
#include "ChildView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CChildView

CChildView::CChildView()
{
}

CChildView::~CChildView()
{
}


BEGIN_MESSAGE_MAP(CChildView, CWnd)
	ON_WM_PAINT()
END_MESSAGE_MAP()



// CChildView 消息处理程序

BOOL CChildView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void CChildView::OnPaint() 
{
	CPaintDC dc(this); // 用于绘制的设备上下文
	
	static BOOL bFirst = TRUE;
	if (bFirst)
	{
		CString sFilePath;
		CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, _T("Describe Files (*.bmp)|*.bmp|All Files (*.*)|*.*||"), NULL);
		if (dlg.DoModal()) {
			sFilePath = dlg.GetPathName();
		}
		else {
			PostQuitMessage(0);
			return;
		}

		// 初始化与图片有关的变量
		m_MemDC.CreateCompatibleDC (&dc);
		m_hBmp = (HBITMAP)LoadImage (NULL, sFilePath, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		if(m_hBmp == INVALID_HANDLE_VALUE) {
			MessageBox (_T("图片打开失败！"), _T("提示信息"), MB_OK);
			PostQuitMessage(0);
			return;
		}
		DeleteObject (m_MemDC.SelectObject(m_hBmp));

		
		CBitmap* pBmp = m_MemDC.GetCurrentBitmap();
		BITMAP stBitmap; 
		pBmp->GetObject (sizeof(BITMAP), &stBitmap);

		if (stBitmap.bmWidth<2 || stBitmap.bmHeight<2)
		{
			MessageBox (_T("指定位置找不到图片，程序即将关闭！"), _T("提示信息"), MB_OK);
			PostQuitMessage (WM_QUIT);
			return;
		}
		else
		{

			m_nBmpWidth  = stBitmap.bmWidth;
			m_nBmpHeight = stBitmap.bmHeight;
			// 根据图片尺寸改变窗口大小
			//int cx = m_nBmpWidth + 200;
			//int cy = m_nBmpHeight + 200;
			//GetParentFrame()->MoveWindow (WINDOW_X, WINDOW_Y, cx, cy, TRUE);
		}
		
		bFirst = FALSE;

		// 启动转换过程
		if (! TransA(sFilePath + _T(".A.txt"))) {
			MessageBox (_T("FOUR_COLOR_IMAGE 文件构造失败！"), _T("失败"), MB_OK);
		}
		if (! TransB(sFilePath + _T(".B.txt"))) {
			MessageBox (_T("BW_IMAGE 文件构造失败！"), _T("失败"), MB_OK );
		}

		// MessageBox (_T("转换完成，按Alt+F4退出程序！"), _T("提示"), MB_OK );
	}

	dc.BitBlt (0,0,m_nBmpWidth,m_nBmpHeight,&m_MemDC,0,0,SRCCOPY);
}


// 生成A文件
BOOL CChildView::TransA(CString sFileName)
{
	HANDLE hf;

	hf = CreateFile(sFileName, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, NULL);
	if(hf == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	DWORD dwWriteLength;

	char object[1024];
	memset (object, 0x0, 1024);
	sprintf (object,
		"static char buffer_A[%d] =\r\n{\r\n",
		(m_nBmpWidth * m_nBmpHeight));

	// 写入开头
	WriteFile (hf, object, strlen(object), &dwWriteLength, NULL);

	for( int i=0; i<m_nBmpHeight; i++ )
	{
		for( int j=0; j<m_nBmpWidth; j++ )
		{
			int color = 0;

			switch (m_MemDC.GetPixel(j, i))
			{
			// 黑色为0
			case RGB(0,0,0):
				color = 0;
				break;
			// 白色为1
			case RGB(255,255,255):
				color = 1;
				break;
			// 绿色为透明色
			case RGB(0,255,0):
				color = 2;
				break;
			// 红色为反色
			case RGB(255,0,0):
				color = 3;
				break;
			// 其他颜色报告错误并退出
			default:
				{
					CloseHandle (hf);
					CString info;
					info.Format(_T("图片格式错误!\n在X=%d，Y=%d位置存在一个非法像素。"), j, i);
					MessageBox (info, _T("失败"), MB_OK);
					return FALSE;
				}
			}

			// 写入内容
			sprintf (object, "%1d,", color);

			if( (i==m_nBmpHeight-1) && (j==m_nBmpWidth-1) )
				WriteFile (hf, object, 1, &dwWriteLength, NULL);
			else
				WriteFile (hf, object, 2, &dwWriteLength, NULL);
		}
		WriteFile (hf, "\r\n", 2, &dwWriteLength, NULL);
	}

	// 写入结尾
	WriteFile (hf, "};\r\n", 4, &dwWriteLength, NULL);

	memset (object, 0x0, 1024);
	sprintf (object,
		"FOUR_COLOR_IMAGE g_4color_A (%d, %d, buffer_A);",
		m_nBmpWidth,
		m_nBmpHeight);

	WriteFile (hf, object, strlen(object), &dwWriteLength, NULL);

	CloseHandle (hf);
	MessageBox (_T("FOUR_COLOR_IMAGE 文件构造成功！"), _T("成功"), MB_OK);

	return TRUE;
}

// 生成B文件
BOOL CChildView::TransB(CString sFileName)
{
	HANDLE hf;

	hf = CreateFile(sFileName, GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, 0, NULL);
	if(hf == INVALID_HANDLE_VALUE)
	{
		return FALSE;
	}

	DWORD dwWriteLength;
	int nWidth = (m_nBmpWidth + 7) / 8;

	// 写入开头
	char object[1024];
	memset (object, 0x0, 1024);
	sprintf (object,
		"BYTE buffer_B [%d] =\r\n{\r\n",
		nWidth * m_nBmpHeight);

	WriteFile (hf, object, strlen(object), &dwWriteLength, NULL);

	int x;
	int y;
	for( y=0; y<m_nBmpHeight; y++ )
	{
		for( x=0; x<nWidth; x++ )
		{
			char strLine[6];
			memset( strLine, 0x0, 6 );

			// 拼凑一个字节
			unsigned char temp = 0x0;

			int c;
			for( c=0; c<8; c++ )
			{
				if (m_MemDC.GetPixel( x*8+c, y ) != 0) {
					// 不是黑色的都算作白色
					temp |= 0x80 >> c;
				}
			}

			if( (y == m_nBmpHeight-1) && (x == nWidth-1) )
				sprintf( strLine, "0x%02x",temp );
			else
				sprintf( strLine, "0x%02x,",temp );

			WriteFile( hf, strLine, 5, &dwWriteLength, NULL );
		}
		WriteFile( hf, &"\r\n", 2, &dwWriteLength, NULL );
	}

	// 写入结尾
	WriteFile (hf, "};\r\n", 4, &dwWriteLength, NULL);

	memset (object, 0x0, 1024);
	sprintf (object,
		"BW_IMAGE bwimg_B (%d, %d, buffer_B);", 
		nWidth * 8,
		m_nBmpHeight);

	WriteFile (hf, object, strlen(object), &dwWriteLength, NULL);

	CloseHandle (hf);
	MessageBox (_T("BW_IMAGE 文件构造成功！"), _T("成功"), MB_OK);

	return TRUE;
}


/* END */