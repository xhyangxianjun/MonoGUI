// PyTransDlg.cpp : implementation file
//

#include "stdafx.h"
#include "PyTrans.h"
#include "PyTransDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPyTransDlg dialog

CPyTransDlg::CPyTransDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPyTransDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPyTransDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CPyTransDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPyTransDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CPyTransDlg, CDialog)
	//{{AFX_MSG_MAP(CPyTransDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CPyTransDlg message handlers

BOOL CPyTransDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPyTransDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPyTransDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

int CPyTransDlg::NewLinkTable()
{
	return TRUE;
}

void CPyTransDlg::OnOK() 
{
	// TODO: Add extra validation here
	HANDLE hf;
	HANDLE hf2;

	// 打开原始拼音库
	hf = CreateFile("gbkpy.txt",GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if(hf==INVALID_HANDLE_VALUE)
	{
		MessageBox ("gbkpy.txt打开失败","失败",MB_OK);
		return;
	}

	// 将拼音库打开到一个数组中
	DWORD dwFileLength = GetFileSize( hf, 0 );
	char* pFile = new char[dwFileLength+1];
	memset (pFile, 0x0, dwFileLength+1);

	DWORD dwActualSize = 0;
	DWORD dwWriteLength = 0;
	BOOL bReadSt;
	bReadSt = ReadFile (hf, pFile, dwFileLength, &dwActualSize, NULL);
	CloseHandle( hf );
	
	if( !bReadSt )
		return;
	if( dwActualSize != dwFileLength )
		return;

	// 创建输出文件
	hf2 = CreateFile("ex.db",GENERIC_WRITE,FILE_SHARE_READ,0,CREATE_ALWAYS,0,NULL);
	if(hf2==INVALID_HANDLE_VALUE)
	{
		MessageBox ("ex.db创建失败","失败",MB_OK);
		return;
	}

	BeginWaitCursor();

	// 逐行解读源拼音库
	char psLine[ 256 ];
	memset( psLine, 0x0, 256 );
	char psTemp[ 4096 ];
	memset( psTemp, 0x0, 4096 );
	int nCur = 0;
	int k = 0;
	while( GetLine( pFile, psLine, k ) )
	{
		// 得到这一行文字空格前面的六个字符（拼音）
		// 得到这一行文字空格后面的两个字符（汉字）
		char psChara[3];
		memset( psChara, 0x0, 3 );
		char psSmb[10];
		memset( psSmb, ' ', 10 );
		for( int i=0; i<7; i++ )
		{
			if( (psSmb[i] = psLine[i]) == ' ' )
			{
				psChara[0] = psLine[ i + 1 ];
				psChara[1] = psLine[ i + 2 ];
				psChara[2] = '\0';
				psSmb[6]   = '\0';
				break;
			}
		}

		// 如果拼音与前面的拼音相同，则将汉字添加在一行后面
		// 如果是不同的拼音，则存入文件，并重新构造一行信息
		if( memcmp( psTemp, psSmb, 6 ) == 0 )
		{
			// 相同，添加之
			memcpy( (psTemp+nCur), psChara, 2 );
			nCur += 2;
			// 出错，超出上限
			if( nCur > 4090 )
				MessageBox( "单行文字长度超出上限！", "出错！", MB_OK );
		}
		else
		{
			// 不同，将一行写入文件
			if( strlen( psTemp ) > 0 )
			{
				psTemp[nCur] = '\n';
				WriteFile( hf2, psTemp, nCur+1, &dwWriteLength, NULL);
			}
			// 然后重新初始该行
			memset( psTemp, 0x0, 4096 );
			memcpy( psTemp, psSmb, 6 );
			memcpy( (psTemp+6), psChara, 2 );
			nCur = 8;
		}

		// 准备进入下一个循环
		memset( psLine, 0x0, 256 );
		k ++;
	}

	CloseHandle (hf2);

	EndWaitCursor();
	CDialog::OnOK();
}

// 从字符串中取得一行
BOOL CPyTransDlg::GetLine(char* cString, char* cReturn, int k)
{
	if( cString == NULL )
		return FALSE;
	
	int iStartPos  = 0;
	int iNextPos   = -1;
	int iStrLength = strlen (cString);

	int i;
	for (i=0; i<=k; i++)
	{
		// 把上次查找的位置给开始位置
		iStartPos += (iNextPos + 1);

		// 查找分隔符'\r'
		if (iStartPos < iStrLength)
		{
			iNextPos = strcspn ((cString+iStartPos), "\n");
		}
		else
		{
			return FALSE;
		}
	}
 
	// 将iStartPos和iEndPos之间的内容拷贝给temp
	char* cTemp = new char [iNextPos + 1];
	memset (cTemp, 0x0, iNextPos+1);
	memcpy (cTemp ,(cString+iStartPos), iNextPos);

	// 如果最末字符是“\r”，则去掉
	iStrLength = iNextPos;
	if (cTemp[iStrLength - 1] == '\r')
	{
		iStrLength --;
	}

	strncpy (cReturn, cTemp, iStrLength);
	cReturn[ iStrLength ] = '\0';

	delete [] cTemp;
	return TRUE;
}

// 从字符串中取得两个“;”之间的部分
BOOL CPyTransDlg::GetSlice(char* cString, char* cReturn, int k)
{
	if(cString == NULL)
		return FALSE;
	int iStartPos  = 0;
	int iNextPos   = -1;
	int iStrLength = strlen (cString);

	int i;
	for (i=0; i<=k; i++)
	{
		// 把上次查找的位置给开始位置（跳过分号）
		iStartPos += (iNextPos + 1);

		// 查找分隔符';'
		if (iStartPos < iStrLength)
		{
			iNextPos = strcspn ((cString+iStartPos), ";");
		}
		else
		{
			return FALSE;
		}
	}
 
	// 将iStartPos和iEndPos之间的内容拷贝给cReturn
	memcpy (cReturn ,(cString+iStartPos), iNextPos);
	cReturn[ iNextPos ] = '\0';

	return TRUE;
}

/* END */