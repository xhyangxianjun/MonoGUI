// MonoGUI.h: interface of the MonoGUI system.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined(__MONOGUI_H__)
#define __MONOGUI_H__

#if defined(RUN_ENVIRONMENT_LINUX)
#include <termios.h>
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
#include <windows.h>
#include <io.h>
#else
#include <unistd.h>
#endif // defined(RUN_ENVIRONMENT_WIN32)

#include <time.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <string.h>
#include <fileapi.h>

// 定义按键宏与按键值对照表的文件名
#define KEY_MAP_FILE "./dtm/keymap.txt"

// 定义当地时区
#define LOCAL_TIME_ZONE         8

// 定义屏幕尺寸
#define SCREEN_W	480
#define SCREEN_H	320

// 系统栏尺寸
#define SYSTEM_BAR_W 64
#define SYSTEM_BAR_H 16

// 滚动条宽度
#define SCROLL_WIDTH 13

// 窗口标题
#define WINDOW_CAPTION_BUFFER_LEN   (256)

// 列表控件
#define LIST_TEXT_MAX_LENGTH	    (256)     // 一行列表框显示内容的长度
#define LIST_ITEM_H                 (HZK_H+1) // 条目的高度

// 消息队列最大深度
#define MESSAGE_MAX                 (60)

// 定时器最大数量
#define TIMER_MAX                   (6)

#define  LENGTH_OF_ASC   3072       /* the number of bytes of the asc font */
#define  LENGTH_OF_HZK   694752     /* the number of bytes of the hzk font */

#if defined (RUN_ENVIRONMENT_LINUX)
#define  ASC_FILE "/usr/local/share/asc12.bin"      /* file name of the asc font */
#define  HZK_FILE "/usr/local/share/gb12song"       /* file name of the hzk font */
#define  PINYIN_LIB_FILE        "/usr/local/share/py.db"
#define  EX_PINYIN_LIB_FILE     "/usr/local/share/ex.db"
#define  LIANXIANG_LIB_FILE     "/usr/local/share/lx.db"
#define  PINYIN_INDEX_FILE      "/usr/local/share/py.idx"
#define  EX_PINYIN_INDEX_FILE   "/usr/local/share/ex.idx"
#define  LIANXIANG_INDEX_FILE   "/usr/local/share/lx.idx"
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
#define  ASC_FILE               "lib/asc12.bin" /* file name of the asc font */
#define  HZK_FILE               "lib/gb12song"  /* file name of the hzk font */
#define  PINYIN_LIB_FILE        "lib/py.db"
#define  EX_PINYIN_LIB_FILE     "lib/ex.db"
#define  LIANXIANG_LIB_FILE     "lib/lx.db"
#define  PINYIN_INDEX_FILE      "lib/py.idx"
#define  EX_PINYIN_INDEX_FILE   "lib/ex.idx"
#define  LIANXIANG_INDEX_FILE   "lib/lx.idx"
#endif // defined(RUN_ENVIRONMENT_WIN32)

#if defined (RUN_ENVIRONMENT_WIN32)
// 仿真窗口的尺寸：1:普通；2:双倍
#define SCREEN_MODE	1
#endif // defined(RUN_ENVIRONMENT_WIN32)

//定义linux专用的GPIO
#if defined (RUN_ENVIRONMENT_LINUX)

#define CLOCK_FOLD     1000
#define GPIO_BASE      0xe0000000
#define KBD_LOCK_BASE  0x0a000000
#define PCCR           0x520
#define PCDR           0x524

#endif // defined(RUN_ENVIRONMENT_LINUX)

#include "OCommon.h"           // 公共全局函数及数组

#include "KEY.h"               // 键盘消息映射表
#include "KeyMap.h"            // 按键宏定义与按键值对照表
#include "BWImgMgt.h"          // 图像资源管理类
#include "LCD.h"               // 液晶屏操作函数(GAL层)
#include "OAccell.h"           // 快捷键
#include "OCaret.h"            // 脱字符
#include "OScrollBar.h"        // 滚动条
#include "OMsgQueue.h"         // 消息队列
#include "OTimerQueue.h"       // 定时器队列
#include "OWindow.h"           // 窗口
#include "OStatic.h"           // 静态文本控件
#include "OButton.h"           // 按钮控件
#include "OEdit.h"             // 编辑框
#include "OList.h"             // 列表框
#include "OCombo.h"            // 组合框
#include "OProgressBar.h"      // 进度条
#include "OImgButton.h"        // 图像按钮
#include "OCheckBox.h"         // 复选框
#include "OIME.h"              // 输入法窗口
#include "OIME_DB.h"           // 输入法数据库
#include "ODialog.h"           // 对话框
#include "OMsgBoxDialog.h"     // MsgBox对话框
#include "OApp.h"              // 主程序
#include "OClock.h"            // 钟表界面
#include "OSystemBar.h"        // 系统状态条


// 定义特殊功能按键（注意，这些定义必须放在KEY.h的后面）
#define KEY_CLOCK          KEY_F2 // 显示/关闭时间窗口
#define KEY_IME_ONOFF      KEY_F3 // 用于打开 / 关闭输入法
#define KEY_IME_PREV       KEY_F4 // 切换到上一种输入法
#define KEY_IME_NEXT       KEY_F5 // 切换到下一种输入法


#endif // !defined(__MONOGUI_H__)

