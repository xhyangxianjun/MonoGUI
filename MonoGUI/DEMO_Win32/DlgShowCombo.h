// DlgShowCombo
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined (__DLGSHOWCOMBO_H__)
#define __DLGSHOWCOMBO_H__

class CDlgShowCombo : public ODialog
{
public:
	CDlgShowCombo();
	virtual ~CDlgShowCombo();

	// 初始化
	void Init();

	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);
};

#endif // !defined(__DLGSHOWCOMBO_H__)