// DlgShowCombo.cpp
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
//////////////////////////////////////////////////////////////////////
CDlgShowCombo::CDlgShowCombo()
{
}

CDlgShowCombo::~CDlgShowCombo()
{
}

// 初始化
void CDlgShowCombo::Init()
{
	OCombo* pCombo = (OCombo*)FindChildByID (102);
	pCombo->AddString ("中国");
	pCombo->AddString ("美国");
	pCombo->AddString ("英国");
	pCombo->AddString ("法国");
	pCombo->AddString ("韩国");
	pCombo->AddString ("俄罗斯");
	pCombo->AddString ("意大利");
	pCombo->AddString ("土耳其");
	pCombo->AddString ("瑞典");
	pCombo->AddString ("丹麦");
	pCombo->AddString ("加拿大");
	pCombo->AddString ("新西兰");
}

// 消息处理过了，返回1，未处理返回0
int CDlgShowCombo::Proc (OWindow* pWnd, int nMsg, int wParam, int lParam)
{
	ODialog::Proc (pWnd, nMsg, wParam, lParam);

	if (pWnd = this)
	{
		if (nMsg == OM_NOTIFY_PARENT)
		{
			switch (wParam)
			{
			case 104:
				{
					OCombo* pCombo = (OCombo*)FindChildByID (102);
					int nSelIndex = pCombo->GetCurSel();
					if (-1 != nSelIndex) {
						char text[LIST_TEXT_MAX_LENGTH];
						if (pCombo->GetString(nSelIndex, text)) {
							char info[LIST_TEXT_MAX_LENGTH + 100];
							sprintf (info, "您选中的内容是：\n%s", text);
							OMsgBox (this, "信息", info,
								OMB_INFORMATION | OMB_SOLID | OMB_ROUND_EDGE, 60);
						}
					}
					else {
						OMsgBox (this, "信息", "请您在下拉列表中做出选择",
							OMB_INFORMATION | OMB_SOLID | OMB_ROUND_EDGE, 60);
					}
				}
				break;

			case 105:
				{
					// 退出按钮
					O_MSG msg;
					msg.pWnd = this;
					msg.message = OM_CLOSE;
					msg.wParam = 0;
					msg.lParam = 0;
					m_pApp->PostMsg (&msg);
				}
				break;
			}
		}
	}

	return 1;
}

/* END */