// Desktop.h: interface for the Desktop class.
// 作者：司徒汇编民间科学工作室，共和国山东青岛
//////////////////////////////////////////////////////////////////////

#if !defined (__DESKTOP_H__)
#define __DESKTOP_H__

class Desktop : public OWindow
{
private:


public:
	Desktop();
	virtual ~Desktop();

	// 虚函数，绘制窗口，只绘制附加的滚动条
	virtual void Paint (LCD* pLCD);

	// 虚函数，消息处理
	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, int nMsg, int wParam, int lParam);

	// 窗口创建后的初始化处理
	virtual void OnInit();

private:
	// 显示功能演示对话框
	void ShowDemoDialog();
};

#endif // !defined(__DESKTOP_H__)
